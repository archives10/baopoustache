require 'json'
require 'open-uri'

class PagesController < ApplicationController
  def home
    url = 'https://discordapp.com/api/guilds/438250504229748746/widget.json'
    discord_json = open(url).read
    discord = JSON.parse(discord_json)
    @invitation = discord["instant_invite"]
  end
end
